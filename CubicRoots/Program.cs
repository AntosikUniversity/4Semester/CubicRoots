﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubicRoots
{
    class Program
    {
        static void Main(string[] args)
        {
            QubicEquation Qgt0 = new QubicEquation(1, 2, 3, 4);           // Q greater than 0
            QubicEquation Qlt0 = new QubicEquation(1, 6, 3, -10);         // Q less than 0
            QubicEquation Qeq0 = new QubicEquation(1, 12, 36, 32);        // Q equals 0*/
            QubicEquation[] equations = { Qgt0, Qlt0, Qeq0 };
            List<string> results = new List<string>();
            foreach (QubicEquation eq in equations)
            {
                eq.SolveCardano(out results);
                foreach (string solution in results)
                    Console.WriteLine(solution);
                Console.WriteLine('\n');
            }
            Console.WriteLine("-------------------");
            foreach (QubicEquation eq in equations)
            {
                eq.SolveViet(out results);
                foreach (string solution in results)
                    Console.WriteLine(solution);
                Console.WriteLine('\n');
            }

            Console.ReadKey();
        }
    }
}
