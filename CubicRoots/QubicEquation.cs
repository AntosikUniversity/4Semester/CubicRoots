﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubicRoots
{
    class QubicEquation
    {
        private double a;
        private double b;
        private double c;
        private double d;

        public double A
        {
            set
            {
                this.a = value;
            }
            get
            {
                return this.a;
            }
        }

        public double B
        {
            set
            {
                this.b = value;
            }
            get
            {
                return this.b;
            }
        }

        public double C
        {
            set
            {
                this.c = value;
            }
            get
            {
                return this.c;
            }
        }

        public double D
        {
            set
            {
                this.d = value;
            }
            get
            {
                return this.d;
            }
        }
        public QubicEquation(int a, int b, int c, int d)
        {
            if (a != 0) A = a;
            else throw new Exception("'a' can't be 0");
            B = b;
            C = c;
            D = d;
        }
        public void SolveCardano(out List<string> solutions)
        {
            // https://ru.wikipedia.org/wiki/%D0%A4%D0%BE%D1%80%D0%BC%D1%83%D0%BB%D0%B0_%D0%9A%D0%B0%D1%80%D0%B4%D0%B0%D0%BD%D0%BE
            double p = 0, q = 0, Q = 0, returns = 0;
            double real = 0, image = 0;
            List<string> s = new List<string>();
            Console.WriteLine(String.Format("Your equation: {0}x^3 + {1}x^2 + {2}x + {3} = 0", A, B, C, D));
            Console.WriteLine("Replace: y = x + b/3a");

            p = (3 * A * C - B * B) / (3 * A * A);
            q = (2 * Math.Pow(B, 3) - (9 * A * B * C) + (27 * A * A * D)) / (27 * Math.Pow(A, 3));
            Q = Math.Pow((p / 3), 3) + Math.Pow((q / 2), 2);
            returns = B / (3 * A);
            if (Q > 0)
            {
                Console.WriteLine("Q > 0   ->   1 Real Root and 2 Complex Roots");
                double alpha = 0, beta = 0;
                alpha = Math.Pow((-q / 2 + Math.Sqrt(Q)), 1.0 / 3);
                beta = -Math.Pow((q / 2 + Math.Sqrt(Q)), 1.0 / 3);

                real = (alpha + beta);
                s.Add(String.Format("x = y - b/3a = ({0} + {1}) - {2} = {3}", alpha, beta, returns, real - returns));

                real = -(alpha + beta) / 2;
                image = (alpha + beta) * Math.Sqrt(3) / 2;
                s.Add(String.Format("x = y - b/3a = -({0} + {1})/2 + i*({0} - {1})/2*sqrt(3) - {2} = {3} + i*{4}", alpha, beta, returns, real - returns, image));

                real = -(alpha + beta) / 2;
                image = (alpha - beta) * Math.Sqrt(3) / 2;
                s.Add(String.Format("x = y - b/3a = -({0} + {1})/2 - i*({0} - {1})/2*sqrt(3) - {2} = {3} - i*{4}", alpha, beta, returns, real - returns, image));
            }
            else if (Q == 0)
            {
                Console.WriteLine("Q = 0   ->   2 Real Root");
                if (p == q && p == 0)
                    s.Add(String.Format("x = -b/3a = {0}", -returns));
                else
                {
                    real = -2 * Math.Pow(q / 2, 1.0 / 3);
                    s.Add(String.Format("x = y - b/3a = {0} - {1} = {2}", real, returns, real - returns));
                    real = Math.Pow(q / 2, 1.0 / 3);
                    s.Add(String.Format("x = y - b/3a = {0} - {1} = {2}", real, returns, real - returns));
                }
            }
            else
            {
                Console.WriteLine("Q < 0   ->   3 Real Root");
                double varphi = 0;
                if (q < 0)
                    varphi = Math.Atan2(-Math.Sqrt(Q), -q / 2);
                else if (q > 0)
                    varphi = Math.Atan2(-Math.Sqrt(Q), -q / 2) + Math.PI;
                else if (q == 0)
                    varphi = Math.PI / 2;
                real = 2 * Math.Sqrt(-p / 3) * Math.Cos(varphi / 3);
                s.Add(String.Format("x = y - b/3a = {0} - {1} = {2}", real, returns, real - returns));
                real = 2 * Math.Sqrt(-p / 3) * Math.Cos(varphi / 3 + 2 * Math.PI / 3);
                s.Add(String.Format("x = y - b/3a = {0} - {1} = {2}", real, returns, real - returns));
                real = 2 * Math.Sqrt(-p / 3) * Math.Cos(varphi / 3 + 4 * Math.PI / 3);
                s.Add(String.Format("x = y - b/3a = {0} - {1} = {2}", real, returns, real - returns));
            }
            solutions = s;
        }
        public void SolveViet(out List<string> solutions)
        {
            // https://ru.wikipedia.org/wiki/%D0%A2%D1%80%D0%B8%D0%B3%D0%BE%D0%BD%D0%BE%D0%BC%D0%B5%D1%82%D1%80%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D1%84%D0%BE%D1%80%D0%BC%D1%83%D0%BB%D0%B0_%D0%92%D0%B8%D0%B5%D1%82%D0%B0
            List<string> s = new List<string>();
            double Q = 0, R = 0, S = 0, varphi = 0;
            double x = 0;
            Q = (A * A - 3 * B) / 9;
            R = ((2 * Math.Pow(A, 3)) - (9 * A * B) + (27 * C)) / 54;
            S = Math.Pow(Q, 3) - Math.Pow(R, 2);
            Console.WriteLine(String.Format("Your equation: {0}x^3 + {1}x^2 + {2}x + {3} = 0", A, B, C, D));
            if (S > 0)
            {
                varphi = Math.Acos(R / Math.Sqrt(Math.Pow(Q, 3))) / 3;

                x = -2 * Math.Sqrt(Q) * Math.Cos(varphi) - A / 3;
                s.Add(String.Format("x = {0}", x));

                x = -2 * Math.Sqrt(Q) * Math.Cos(varphi + 2.0 / 3 * Math.PI) - A / 3;
                s.Add(String.Format("x = {0}", x));

                x = -2 * Math.Sqrt(Q) * Math.Cos(varphi - 2.0 / 3 * Math.PI) - A / 3;
                s.Add(String.Format("x = {0}", x));

            }
            else if (S < 0)
            {
                double real = 0;
                double img = 0;
                if (Q > 0)
                {
                    varphi = this.Arch(Math.Abs(R) / Math.Pow(Math.Abs(Q), 3.0 / 2)) / 3;

                    x = -2 * Math.Sign(R) * Math.Sqrt(Math.Abs(Q)) * Math.Cosh(varphi) - A / 3;
                    s.Add(String.Format("x = {0}", x));

                    real = Math.Sign(R) * Math.Sqrt(Math.Abs(Q)) * Math.Cosh(varphi) - A / 3;
                    img = Math.Sqrt(3) * Math.Sqrt(Math.Abs(Q)) * Math.Sinh(varphi);
                    s.Add(String.Format("x = {0} + i*{1}", real, img));
                    s.Add(String.Format("x = {0} - i*{1}", real, img));
                }
                else if (Q < 0)
                {
                    varphi = this.Arsh(Math.Abs(R) / Math.Pow(Math.Abs(Q), 3.0 / 2)) / 3;

                    x = -2 * Math.Sign(R) * Math.Sqrt(Math.Abs(Q)) * Math.Sinh(varphi) - A / 3;
                    s.Add(String.Format("x = {0}", x));

                    real = Math.Sign(R) * Math.Sqrt(Math.Abs(Q)) * Math.Sinh(varphi) - A / 3;
                    img = Math.Sqrt(3) * Math.Sqrt(Math.Abs(Q)) * Math.Cosh(varphi);
                    s.Add(String.Format("x = {0} + i*{1}", real, img));
                    s.Add(String.Format("x = {0} - i*{1}", real, img));
                }
                else
                {
                    x = -Math.Pow(C - Math.Pow(A, 3) / 27, 1.0 / 3) - A / 3;
                    s.Add(String.Format("x = {0}", x));

                    real = -(A + x) / 2;
                    img = (Math.Sqrt(Math.Abs((A - 3 * x) * (A + x) - 4 * B))) / 2;
                    s.Add(String.Format("x = {0} + i*{1}", real, img));
                    s.Add(String.Format("x = {0} - i*{1}", real, img));
                }
            }
            else
            {
                x = -2 * Math.Sign(R) * Math.Sqrt(Q) - A / 3;
                s.Add(String.Format("x = {0}", x));

                x = Math.Sign(R) * Math.Sqrt(Q) - A / 3;
                s.Add(String.Format("x = {0}", x));
            }


            solutions = s;
        }
        private double Arsh(double x)
        {
            return Math.Log(x + Math.Sqrt(x * x + 1.0));
        }
        private double Arch(double x)
        {
            return Math.Log(x + Math.Sqrt(x * x - 1.0));
        }
    }
}
